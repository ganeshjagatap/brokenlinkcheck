package Scripts;
import java.awt.Desktop;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import net.continuumsecurity.proxy.ScanningProxy;
import net.continuumsecurity.proxy.Spider;
import net.continuumsecurity.proxy.ZAProxyScanner;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GeneralMethods.ProjectSpecific;
import GeneralMethods.ReUsableMethods;


public class LinkCheckerFlexiEMR {
	private ScanningProxy zapScanner;
	 private Spider zapSpider;
	public static Set<String> allPages;
	public static Set<String> allLinks;
	public static WebDriver driver;
	public static int invalidLinksCount;
	public static Set<String> brokenPages;
	public static final String FILENAME = "E:\\security\\zap\\URL_StatusOfFlexiEMR.txt";
	public static List<Float> loadingTimeInSecond= new ArrayList<Float>();
	public static List<Long> loadingTimeInMSecond= new ArrayList<Long>();
	public static String pieChartLocOfFlexiEMR= "E:\\workspace\\brokenlinkcheck\\test-output\\Broken_Links_Tata_Articles\\test-output\\PieChartOfFlexiEMR.html";
	public static String columnChartLocOfFlexiEMR= "E:\\workspace\\brokenlinkcheck\\test-output\\Broken_Links_Tata_Articles\\test-output\\ColumnChartOfFlexiEMR.html";
	
	@BeforeMethod
	public void openBrowserURL() throws InterruptedException
	{
		 try {
		        //Process p =  Runtime.getRuntime().exec("cmd /c start \"E:\\security\\zap\\run.bat\"") ; 
			  String[] command = {"cmd.exe", "/C", "Start", "E:\\security\\zap\\run.bat"};
	          Process p =  Runtime.getRuntime().exec(command);
		    } catch (IOException ex) {
		    }
		  Thread.sleep(10000);
        Proxy proxy = new Proxy();
        proxy.setHttpProxy("127.0.0.1:8888");
        proxy.setFtpProxy("127.0.0.1:8888");
        proxy.setSslProxy("127.0.0.1:8888");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(CapabilityType.PROXY, proxy);
        System.setProperty("webdriver.chrome.driver", "E:\\workspace\\webautomationrepo\\chromedriver_win32_2.1\\chromedriver.exe");
      driver = new ChromeDriver(capabilities);
        zapScanner = new ZAProxyScanner("127.0.0.1",8888,"qn8mqnf88h6jrmi3ijotirpr15");
        //qn8mqnf88h6jrmi3ijotirpr15
        //zapScanner.
        //5blusbrhpn4i6psn9m7csr09qh
        //alofieiklmai4195jjm3io54lh
        //zapScanner.
        //zapScanner.clear(); //Start a new session
       // zapScanner.setEnablePassiveScan(true);
        //zapapi.core.htmlreport();
        //zapScanner.getHtmlReport();
        zapSpider = (Spider)zapScanner;
        
       // zapScanner.scan(null);
       //log.info("Created client to ZAP API");
       // this.setDriver(driver);
        System.out.println("test");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get("https://test.tatahealth.com/flexiemr-website/public");
        Thread.sleep(5000);
       // zapScanner.scan("https://test.tatahealth.com/flexiemr-website/public");
		//driver.get("https://test.tatahealth.com/flexiemr-website/public/");
		driver.findElement(By.id("txtUsername")).sendKeys("mala");
		driver.findElement(By.id("txtPassword")).sendKeys("Test123@");
		
		driver.findElement(By.id("login")).click();
		Thread.sleep(5000);
		try{
		driver.findElement(By.xpath("//button[@class='confirm']")).click();
		}catch(Exception e)
		{
			
		}
		Thread.sleep(50000);
	}
	
	@AfterMethod
	public void closeBrowser() throws InterruptedException, Exception
	{
		
		zapScanner.clear();
		zapScanner.shutdown();
		Thread.sleep(10000);
		 try {
		        //Process p =  Runtime.getRuntime().exec("cmd /c start \"E:\\security\\zap\\run.bat\"") ; 
			  String[] command = {"cmd.exe", "/C", "Start", "E:\\security\\zap\\runreport.bat"};
	          Process p =  Runtime.getRuntime().exec(command);
		    } catch (IOException ex) {
		    }
		
		
		String htmlFilePath ="E:\\security\\ZAP_2.5.0\\report.html"; // path to your new file
		System.out.println("path "+htmlFilePath);
		File htmlFile = new File(htmlFilePath);
		Desktop.getDesktop().browse(htmlFile.toURI());
		if(driver!= null)
			driver.quit();
	}
		
	
	@Test
	public void FindBrokenLinks() throws Exception
	{
		System.out.println(driver.getCurrentUrl());
		Reporter.log(driver.getCurrentUrl());
		brokenPages= new HashSet<String>();
		allLinks= new HashSet<String>();
		BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME));
		ReUsableMethods.getAllPages(ProjectSpecific.getMajorWebPagesOfFlexiEmr(), driver, allPages, invalidLinksCount, allLinks);
		System.out.println("Total links are:"+allLinks.size());
		for(String str:allLinks)
			{
			try{
				if(!str.contains("https://test.tatahealth.com/flexiemr-website/public/logout"))
				driver.get(str);
			}catch(Exception e){
				System.out.println(e.getMessage());
			}
				//WebPages.validateInvalidLinks();
				ReUsableMethods.verifyURLStatus(str, driver, bw, brokenPages, loadingTimeInSecond, loadingTimeInMSecond);
				
			}
			System.out.println("Total count of broken links are: "+brokenPages.size());
			Reporter.log("Total count of broken links are: "+brokenPages.size());
			bw.write("Total count of broken links are: "+brokenPages.size());
			if(brokenPages.size()>0){
			System.out.println("Broken links are as below:");
			Reporter.log("Broken links are as below:");
			bw.newLine();
			bw.write("Broken links are as below:");
			bw.newLine();
			for(String str1:brokenPages)
			{
			System.out.println("URL: "+str1);
			Reporter.log("URL: "+str1);
			bw.write("URL: "+str1);
			bw.newLine();
			
			}
			}
			ReUsableMethods.drawColumnChart(loadingTimeInSecond, loadingTimeInMSecond, columnChartLocOfFlexiEMR);
			ReUsableMethods.drawPieChart(allLinks, brokenPages, pieChartLocOfFlexiEMR);
			Reporter.log("<a href=\""+pieChartLocOfFlexiEMR+"\">pieChartLoc</a>");
			Reporter.log("<a target=\"_blank\" href=\""+columnChartLocOfFlexiEMR+"\">columnChartLoc</a>");
			Reporter.log("<img src=\""+pieChartLocOfFlexiEMR+"\">pieChartLoc</a>");
			
			bw.close();
		}
		
	}
	
	
