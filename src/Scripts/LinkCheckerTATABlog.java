package Scripts;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GeneralMethods.ProjectSpecificMethods;
import GeneralMethods.ReUsableMethods;


public class LinkCheckerTATABlog {

	public static List<String> allPages;
	public static List<String> allLinks;
	public static WebDriver driver;
	public static int invalidLinksCount;
	public static Set<String> brokenPages;
	public static final String FILENAME = System.getProperty("user.dir")+"\\URL_StatusOfTATABlog.txt";
	public static final String HighPageLoadTime = System.getProperty("user.dir")+"\\PagesWithHighLoadTime.txt";
	public static List<Float> loadingTimeInSecond= new ArrayList<Float>();
	public static List<Float> loadingTimeInMSecond= new ArrayList<Float>();
	public static List<String> highLoadTimePageURL= new ArrayList<>();
	public static String pieChartLocOfTATABlog= System.getProperty("user.dir")+"\\test-output\\PieChartTataBlog.html";
	public static String columnChartLocOfTATABlog= System.getProperty("user.dir")+"\\test-output\\ColumnChartTataBlog.html";
	
	@BeforeMethod
	public void openBrowserURL() throws InterruptedException
	{
		System.out.println(System.getProperty("user.dir"));
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\exefiles\\chromedriver1.exe");
		driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//driver.get("https://test.tatahealth.com/blog/");
	}
	
	@AfterMethod
	public void closeBrowser()
	{
		if(driver!= null)
			driver.quit();
	}
		
	
	@Test
	public void FindBrokenLinks() throws Exception
	{
		System.out.println(driver.getCurrentUrl());
		Reporter.log(driver.getCurrentUrl());
		brokenPages= new HashSet<String>();
		allLinks= new ArrayList<String>();
		BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME));
		//ReUsableMethods.getAllPages(ProjectSpecific.getMajorWebPages(), driver, allPages, invalidLinksCount, allLinks);
		allLinks= ProjectSpecificMethods.GenerateCSVFileFromBlog(driver);
		System.out.println("Total links are:"+allLinks.size());
		for(String str:allLinks)
			{
				ReUsableMethods.verifyURLStatus(str, driver, bw, brokenPages, loadingTimeInSecond, loadingTimeInMSecond, highLoadTimePageURL);
				
			}
			System.out.println("Total count of broken links are: "+brokenPages.size());
			Reporter.log("Total count of broken links are: "+brokenPages.size());
			bw.write("Total count of broken links are: "+brokenPages.size());
			if(brokenPages.size()>0){
			System.out.println("Broken links are as below:");
			Reporter.log("Broken links are as below:");
			bw.newLine();
			bw.write("Broken links are as below:");
			bw.newLine();
			for(String str1:brokenPages)
			{
			System.out.println("URL: "+str1);
			Reporter.log("URL: "+str1);
			bw.write("URL: "+str1);
			bw.newLine();
			
			}
			}
			BufferedWriter bw1 = new BufferedWriter(new FileWriter(HighPageLoadTime));
			bw1.write("Total count of pages with high loading time:"+highLoadTimePageURL.size());
			bw1.newLine();
			for(String s: highLoadTimePageURL)
			{
				bw1.write(s);
				bw1.newLine();
			}
			bw1.close();
			ReUsableMethods.drawColumnChart(loadingTimeInSecond, loadingTimeInMSecond, columnChartLocOfTATABlog, allLinks.size(), brokenPages.size(), FILENAME, HighPageLoadTime);
			ReUsableMethods.drawPieChart(allLinks, brokenPages, pieChartLocOfTATABlog);
			Reporter.log("<a href=\""+pieChartLocOfTATABlog+"\">pieChartLoc</a>");
			Reporter.log("<a target=\"_blank\" href=\""+columnChartLocOfTATABlog+"\">columnChartLoc</a>");
			Reporter.log("<img src=\""+pieChartLocOfTATABlog+"\">pieChartLoc</img>");
			
			bw.close();
			
			ReUsableMethods.openhtml();
		}
		
	}
	
	
