package GeneralMethods;

import java.util.HashSet;
import java.util.Set;

public class ProjectSpecific {

	
	
	public static Set<String> getMajorWebPages()
	{
		Set<String> allMajorPages= new HashSet<>();
		allMajorPages.add("https://test.tatahealth.com/blog/");
		allMajorPages.add("https://test.tatahealth.com/blog/category/everyday-health/");
		allMajorPages.add("https://test.tatahealth.com/blog/category/diabetes/");
		allMajorPages.add("https://test.tatahealth.com/blog/category/pregnancy/");
		allMajorPages.add("https://test.tatahealth.com/blog/category/womens-health/");
		allMajorPages.add("https://test.tatahealth.com/blog/category/well-being/");
		allMajorPages.add("https://test.tatahealth.com/blog/forums/");
		
	    return allMajorPages;
	}
	
	public static Set<String> getMajorWebPagesOfFlexiEmr()
	{
		Set<String> allMajorPages= new HashSet<>();
		allMajorPages.add("https://test.tatahealth.com/flexiemr-website/public/appointment");
		allMajorPages.add("https://test.tatahealth.com/flexiemr-website/public/billing");
		allMajorPages.add("https://test.tatahealth.com/flexiemr-website/public/userAdministration");
		allMajorPages.add("https://test.tatahealth.com/flexiemr-website/public/reports");
		allMajorPages.add("https://test.tatahealth.com/flexiemr-website/public/masterAdministration");
		allMajorPages.add("https://test.tatahealth.com/flexiemr-website/public/templateMaster");
		allMajorPages.add("https://test.tatahealth.com/flexiemr-website/public/openConsultation");
		
	    return allMajorPages;
	}
	
}
