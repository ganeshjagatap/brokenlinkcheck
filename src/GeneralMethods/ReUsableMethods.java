package GeneralMethods;

import java.awt.Desktop;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.poi.ss.usermodel.Workbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import com.opencsv.CSVReader;

//import com.articles.tata.NewOne;

public class ReUsableMethods {

	public static Workbook wb;
	
	// Method to calculate page load time in second and millisecond
	public static void pageLoadTime(String URL, WebDriver driver, BufferedWriter bw, List<Float> loadingTimeInSecond, List<Float> loadingTimeInMSecond, List<String> highPageLoad) throws Exception
	{
		
		StopWatch pageLoad = new StopWatch();
        pageLoad.start();
        driver.get(URL);
        pageLoad.stop();
        float pageLoadTime_ms = pageLoad.getTime();
        float pageLoadTime_Seconds =(pageLoadTime_ms / 1000);
        if(pageLoadTime_Seconds > 1)
        	highPageLoad.add(URL);
        System.out.println("Total Page Load Time: " + pageLoadTime_ms + " milliseconds");
        System.out.println("Total Page Load Time: " + pageLoadTime_Seconds + " seconds");
        Reporter.log("Total Page Load Time: " + pageLoadTime_ms + " milliseconds");
        Reporter.log("Total Page Load Time: " + pageLoadTime_Seconds + " seconds");
        bw.newLine();
        bw.write("Total Page Load Time: " + pageLoadTime_ms + " milliseconds");
        bw.newLine();
        bw.write("Total Page Load Time: " + pageLoadTime_Seconds + " seconds");
        bw.newLine();
        bw.write("===============================================================");
        bw.newLine();
        bw.flush();
        loadingTimeInSecond.add(pageLoadTime_Seconds);
        loadingTimeInMSecond.add(pageLoadTime_ms);
        
        
	}
	
	
	// method to verify URL status
	
	public static void verifyURLStatus(String URL, WebDriver driver, BufferedWriter bw, Set<String> brokenPages, List<Float> loadingTimeInSecond, List<Float> loadingTimeInMSecond, List<String>HighLoadTime) throws Exception {

		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(URL);
		
		try {
			HttpResponse response = client.execute(request);
			if (response.getStatusLine().getStatusCode() != 200)
			{
				System.out.println(response.getStatusLine().getStatusCode());
				brokenPages.add(URL);
			}
			else
			{
				System.out.println("URL: "+URL);
				System.out.println("Status: "+response.getStatusLine().getStatusCode());
				Reporter.log("URL: "+URL);
				Reporter.log("Status: "+response.getStatusLine().getStatusCode());
				//WebPages.pageLoadTime(URL, driver);
				//System.out.println("====================================================");
				bw.write("URL: "+URL);
				bw.newLine();
				bw.write("Status: "+response.getStatusLine().getStatusCode());
				//bw.newLine();
				ReUsableMethods.pageLoadTime(URL, driver, bw,loadingTimeInSecond, loadingTimeInMSecond , HighLoadTime);
				
				
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	// method to draw barchart
	
	public static void drawPieChart(List<String> allLinks, Set<String> brokenPages, String pieChartLoc) throws Exception
	{
		BufferedWriter bw1 = new BufferedWriter(new FileWriter(pieChartLoc));
	      bw1.write("<html>");
	      bw1.write("<head>");
	      bw1.write("<script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>");
	      bw1.write("<script type=\"text/javascript\"> "
	      		+ "google.charts.load('current', {'packages':['corechart']});"
	      		+ "google.charts.setOnLoadCallback(drawChart);"
	      		+ "function drawChart() {"
	      		+ "var data = google.visualization.arrayToDataTable(["
	      		+ "['Task', 'Hours per Day'],"
	      		+ "['BrokenPages',     "+brokenPages.size()+"],"
	      		+ "['WorkingPages',   "+(allLinks.size()-brokenPages.size())+"]]);"
	      		+ " var options = { "
	            + " title: 'Status  of links'};"
	            + " var chart = new google.visualization.PieChart(document.getElementById('piechart')); "
	            + "chart.draw(data, options); "
	            + "} "
	            + "</script>");
	      bw1.write("</head>");
	      bw1.write("<body><div id=\"piechart\" style=\"width: 900px; height: 500px;\"></div</body>");
	      bw1.write("/html");
	      bw1.close();
	}
	
	
	public static void drawColumnChart(List<Float> loadingTimeInSecond, List<Float> loadingTimeInMSecond, String columnChartLoc, int pageCount, int brokenPagesCount, String filename, String highLoad) throws Exception
	{
		Collections.sort(loadingTimeInSecond);
		float maxPageLoadTimeInSeconds=loadingTimeInSecond.get(loadingTimeInSecond.size()-1);
		//float minPageLoadTimeInSeconds=loadingTimeInSecond.get(0);
		Collections.sort(loadingTimeInMSecond);
		Float maxPageLoadTimeInMSeconds=loadingTimeInMSecond.get(loadingTimeInMSecond.size()-1);
		Float minPageLoadTimeInMSeconds=loadingTimeInMSecond.get(0);
		
		float sumInSeconds=0;
		float sumInMSeconds=0;
		for(Float f:loadingTimeInSecond)
		{
			sumInSeconds=sumInSeconds+f;
		}
		//float avgInSeconds= sumInSeconds/loadingTimeInSecond.size();
		
		for(Float f:loadingTimeInMSecond)
		{
			sumInMSeconds=sumInMSeconds+f;
		}
		float avgInMSeconds= sumInMSeconds/loadingTimeInMSecond.size();
		
		BufferedWriter bw1 = new BufferedWriter(new FileWriter(columnChartLoc));
	      bw1.write("<html>");
	      bw1.write("<head>");
	      bw1.write("<script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>");
	      bw1.write("<script type=\"text/javascript\"> "
	      		+ "google.charts.load(\"current\", {packages:['corechart']});"
	      		+ "google.charts.setOnLoadCallback(drawChart);"
	      		+ "function drawChart() {"
	      		+ "var data = google.visualization.arrayToDataTable(["
	      		+ "[\"ColumnsName\", \"Values\", { role: \"style\" } ],"
	      		//+ "[\"maxInSec\", "+maxPageLoadTimeInSeconds+", \"#b87333\"],"
	      		//+ "[\"minInSec\", "+minPageLoadTimeInSeconds+", \"color: silver\"] , "
	      		+ "[\"maxInMS\", "+maxPageLoadTimeInMSeconds+", \"color: gold\"] ,"
	      		+ "[\"minInMS\", "+minPageLoadTimeInMSeconds+", \"color: red\"] , "
	      		//+ "[\"avgInSeconds\", "+avgInSeconds+", \"color: yellow\"] , "
	      		+ "[\"avgInMSeconds\", "+avgInMSeconds+", \"color: #e5e4e2\"]]);"
	      		+ "var view = new google.visualization.DataView(data); "
	            + "view.setColumns([0, 1,"
	      		+ "{ calc: \"stringify\","
	            + " sourceColumn: 1,"
	      		+ " type: \"string\","
	      		+ " role: \"annotation\" },"
	      		+ " 2]);"
	      		+ " var options = { "
	            + " title: \"Page Load Time\","
	      		+ " width: 600,"
	            + " height: 400,"
	      		+ " bar: {groupWidth: \"95%\"},"
	            + " };"
	            + " var chart = new google.visualization.ColumnChart(document.getElementById(\"columnchart_values\")); "
	            + "chart.draw(view, options); "
	            + "}"
	            + "</script>");
	      bw1.write("</head>");
	      bw1.write("<body><div id=\"columnchart_values\" style=\"width: 900px; height: 500px;\"></div>");
	      bw1.write("<p>Total number of pages is: "+pageCount);
	      bw1.write("<p>Total number of broken pages is: "+brokenPagesCount);
	      bw1.write("<br>");
	      bw1.write("<p><a href=\""+filename+"\">DetailedReportLink</a></p>");
	      bw1.write("<p><a href=\""+highLoad+"\">PagesWithHighLoadTime</a></p>");
	      bw1.write("</body>");
	      bw1.write("</html>");
	      bw1.close();
	}
	
	
	public static void openhtml() throws Exception
    {
        String htmlFilePath =System.getProperty("user.dir") +"/test-output/ColumnChartTataBlog.html"; // path to your new file
        System.out.println("path "+htmlFilePath);
        File htmlFile = new File(htmlFilePath);
        Desktop.getDesktop().browse(htmlFile.toURI());
    }
	
	
	public static List<String> readAllDataOfColumn(File filePath)
	{
		FileReader reader=null;
		List<String> allURLs= new ArrayList<String>();
	
		try {
			reader= new FileReader(filePath);
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		
		CSVReader csv= new CSVReader(reader);
		List<String[]> list = null;
		try {
			list = csv.readAll();
		} catch (IOException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
		Iterator<String[]> it= list.iterator();
		while(it.hasNext())
		{
			String str[]= it.next();
			for(String s: str)
			{
				//System.out.println(s);
				/*if(!s.isEmpty() && s !=null)
				allURLs.add(s);*/
				
				if(s != null)
				{
				if(!s.isEmpty() && !s.contains("URLs"))
				{
					allURLs.add(s);
				}
				}

				
			}
		}
		return allURLs;
		
		
		
		
	}
	
	
	public static WebElement expandRootElement(WebElement element, WebDriver driver) {
		WebElement ele = (WebElement) ((JavascriptExecutor) driver).executeScript("return arguments[0].shadowRoot",element);
		return ele;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	}
