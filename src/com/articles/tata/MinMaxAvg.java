package com.articles.tata;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.testng.internal.junit.ArrayAsserts;

public class MinMaxAvg {

	public static void main(String[] args) {
		
		ArrayList<Float> a1= new ArrayList<>();
		a1.add(12.25f);
		a1.add(12.23f);
		a1.add(12.27f);
		a1.add(12.24f);
		a1.add(12.26f);
		System.out.println(a1);
		Collections.sort(a1);
		System.out.println(a1);
		System.out.println(a1.get(0));
		System.out.println(a1.get(a1.size()-1));
		float sum=0;
		for(Float f:a1)
		{
			sum=sum+f;
		}
		System.out.println(sum/a1.size());
	}
}
