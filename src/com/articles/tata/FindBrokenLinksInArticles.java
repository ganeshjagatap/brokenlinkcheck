package com.articles.tata;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;





public class FindBrokenLinksInArticles {

	public static Set<String> allPages;
	public static Set<String> allLinks;
	public static WebDriver driver;
	public static int invalidLinksCount;
	public static Set<String> brokenPages;
	public static final String FILENAME = "E:\\ContentTestReport.txt";
	
	
	public static void pageLoadTime(String URL, WebDriver driver, BufferedWriter bw) throws Exception
	{
		
		StopWatch pageLoad = new StopWatch();
        pageLoad.start();
        driver.get(URL);
        pageLoad.stop();
        long pageLoadTime_ms = pageLoad.getTime();
        float pageLoadTime_Seconds = pageLoadTime_ms / 1000;
        System.out.println("Total Page Load Time: " + pageLoadTime_ms + " milliseconds");
        System.out.println("Total Page Load Time: " + pageLoadTime_Seconds + " seconds");
        bw.newLine();
        bw.write("Total Page Load Time: " + pageLoadTime_ms + " milliseconds");
        bw.newLine();
        bw.write("Total Page Load Time: " + pageLoadTime_Seconds + " seconds");
        bw.newLine();
        bw.write("===============================================================");
        bw.newLine();
        bw.flush();
        
	}
	
	public static void validateInvalidLinks() {

		try {
			invalidLinksCount = 0;
			List<WebElement> anchorTagsList = null;
			try{
				//driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
				//Thread.sleep(10000);
				anchorTagsList = driver.findElements(By.xpath("//a"));
			
			//System.out.println(d);
			}catch(Exception e)
			{
				System.out.println("dfasf");
				e.printStackTrace();
			}
			System.out.println("Total no. of links are "+ anchorTagsList.size());
			for (WebElement anchorTagElement : anchorTagsList) {
				if (anchorTagElement != null) {
					String url = anchorTagElement.getAttribute("href");
					
					// href with javascript is not link.
					if (url != null && !url.contains("javascript:void(0)")) {
						//verifyURLStatus(url);
						allLinks.add(url);
						System.out.println("list of url "+url);	
					} else {
						invalidLinksCount++;
					}
				}
			}

			//System.out.println("Total no. of invalid links are "+ invalidLinksCount);

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
	
	
	public static void verifyURLStatus(String URL, WebDriver driver, BufferedWriter bw) throws Exception {

		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(URL);
		
		try {
			HttpResponse response = client.execute(request);
			if (response.getStatusLine().getStatusCode() != 200)
			{
				System.out.println(response.getStatusLine().getStatusCode());
				brokenPages.add(URL);
			}
			else
			{
				System.out.println("URL: "+URL);
				System.out.println("Status: "+response.getStatusLine().getStatusCode());
				//WebPages.pageLoadTime(URL, driver);
				//System.out.println("====================================================");
				bw.write("URL: "+URL);
				bw.newLine();
				bw.write("Status: "+response.getStatusLine().getStatusCode());
				//bw.newLine();
				FindBrokenLinksInArticles.pageLoadTime(URL, driver, bw);
				
				
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static Set<String> getMajorWebPages()
	{
		Set<String> allMajorPages= new HashSet<>();
		allMajorPages.add("https://test.tatahealth.com/blog/");
		allMajorPages.add("https://test.tatahealth.com/blog/category/everyday-health/");
		allMajorPages.add("https://test.tatahealth.com/blog/category/diabetes/");
		allMajorPages.add("https://test.tatahealth.com/blog/category/pregnancy/");
		allMajorPages.add("https://test.tatahealth.com/blog/category/womens-health/");
		//allMajorPages.add("https://test.tatahealth.com/blog/category/well-being/");
		allMajorPages.add("https://test.tatahealth.com/blog/forums/");
		
	    return allMajorPages;
	}
	public static void getAllPages(Set<String> MajorWebPages, WebDriver driver) throws Exception
	{
		allPages= new HashSet<String>();
		if(MajorWebPages.size()==0)
		{
			System.out.println("No web pages found");
			//return null;
		}
		else
		{
			for(String webpage:MajorWebPages)
			{
				System.out.println("Current page:"+webpage);
				boolean flag=false;
				allPages.add(webpage);
				driver.get(webpage);
				Thread.sleep(4000);
				FindBrokenLinksInArticles.validateInvalidLinks();
				try{
				do{
			//	WebElement next= driver.findElement(By.xpath("//nav[@class='cb-page-navigation']/ul[@class='page-numbers']//li/a/i[@class='fa fa-long-arrow-right']"));
				//flag=true;
			//	next.click();
				Thread.sleep(1000);
				FindBrokenLinksInArticles.validateInvalidLinks();
				String nextPageURL= driver.getCurrentUrl();
				//String nextPageURL= driver.findElement(By.xpath("//nav[@class='cb-page-navigation']/ul[@class='page-numbers']//li/a")).getAttribute("href");
				allPages.add(nextPageURL);
				}while(flag==true);
				}catch(Exception e)
				{
					System.out.println("NOt found");
					flag=false;
				}
				
			}
			//return allPages;
		}
	}
		
		public static void main(String args[]) throws Exception
		{
			System.setProperty("webdriver.chrome.driver", "E:\\workspace\\webautomationrepo\\chromedriver_win32_2.1\\chromedriver.exe");
			driver= new ChromeDriver();
			//driver.get("https://articles.tatahealth.com/");
			//HtmlUnitDriver driver= new HtmlUnitDriver();
			//driver.setJavascriptEnabled(true);
			//System.setProperty("webdriver.chrome.driver", "C:\\Users\\Amod Mahajan\\workspace\\Checking_Links_Page\\exefiles\\phantomjs.exe");
			//driver= new ChromeDriver();
			//driver = new PhantomJSDriver();
			//java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.OFF);
		    //java.util.logging.Logger.getLogger("org.apache.http").setLevel(java.util.logging.Level.OFF);
			driver.get("https://test.tatahealth.com/blog/");
			//driver.manage().window().maximize();
			
			
			System.out.println(driver.getCurrentUrl());
			//Set<String> pages= WebPages.getAllPages(getMajorWebPages(), driver);
			//System.out.println(pages);
			brokenPages= new HashSet<String>();
			allLinks= new HashSet<String>();
			BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME));
			FindBrokenLinksInArticles.getAllPages(getMajorWebPages(), driver);
			System.out.println("Total links are:"+allLinks.size());
			for(String str:allLinks)
			{
				System.out.println("url "+str);
				driver.get(str);
				//WebPages.validateInvalidLinks();
				FindBrokenLinksInArticles.verifyURLStatus(str, driver, bw);
				
			}
			System.out.println("Total count of broken links are: "+brokenPages.size());
			bw.write("Total count of broken links are: "+brokenPages.size());
			if(brokenPages.size()>0){
			System.out.println("Broken links are as below:");
			bw.newLine();
			bw.write("Total count of broken links are: "+brokenPages.size());
			bw.newLine();
			for(String str1:brokenPages)
			{
			System.out.println(str1);
			
			bw.write("URL: "+str1);
			bw.newLine();
			}
			}
			
			driver.quit();
			bw.close();
		}
		
	}
	
	
