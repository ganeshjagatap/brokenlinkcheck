package com.articles.tata;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel; 
import org.jfree.chart.JFreeChart; 
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset; 
import org.jfree.data.category.DefaultCategoryDataset; 
import org.jfree.ui.ApplicationFrame; 
import org.jfree.ui.RefineryUtilities; 

public class BarChart extends ApplicationFrame
{
   public BarChart( String applicationTitle , String chartTitle )
   {
      super( applicationTitle );        
      JFreeChart barChart = ChartFactory.createBarChart(
         chartTitle,           
         "Category",            
         "Score",            
         createDataset(),          
         PlotOrientation.VERTICAL,           
         true, true, false);
         
      ChartPanel chartPanel = new ChartPanel( barChart );        
      chartPanel.setPreferredSize(new java.awt.Dimension( 560 , 367 ) );        
      setContentPane( chartPanel ); 
   }
   private CategoryDataset createDataset( )
   {
      final String fiat = "FIAT";        
      final String audi = "AUDI";        
      final String ford = "FORD";        
      final String speed = "Speed";        
      final String millage = "Millage";        
      final String userrating = "User Rating";        
      final String safety = "safety";        
      final DefaultCategoryDataset dataset = 
      new DefaultCategoryDataset( );  

      dataset.addValue( 1.0 , fiat , speed );        
      dataset.addValue( 3.0 , fiat , userrating );        
      dataset.addValue( 5.0 , fiat , millage ); 
      dataset.addValue( 5.0 , fiat , safety );           

      dataset.addValue( 5.0 , audi , speed );        
      dataset.addValue( 6.0 , audi , userrating );       
      dataset.addValue( 10.0 , audi , millage );        
      dataset.addValue( 4.0 , audi , safety );

      dataset.addValue( 4.0 , ford , speed );        
      dataset.addValue( 2.0 , ford , userrating );        
      dataset.addValue( 3.0 , ford , millage );        
      dataset.addValue( 6.0 , ford , safety );               

      return dataset; 
   }
   public static void main( String[ ] args ) throws IOException
   {
	  //BarChart chart = new BarChart("Car Usage Statistics", "Which car do you like?");
      //chart.pack( );        
      //RefineryUtilities.centerFrameOnScreen( chart );        
      //chart.setVisible( true );
      //StringBuilder htmlStringBuilder=new StringBuilder();
      //htmlStringBuilder.append("<html><head><title>Selenium Test </title></head>");
      //htmlStringBuilder.append("<body>");
      //htmlStringBuilder.append(chart);
      //htmlStringBuilder.append("</table></body></html>");
      BufferedWriter bw1 = new BufferedWriter(new FileWriter("C:\\Users\\Amod Mahajan\\workspace\\Broken_Links_Tata_Articles\\Summary.html"));
      bw1.write("<html>");
      bw1.write("<head>");
      bw1.write("<script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>");
      bw1.write("<script type=\"text/javascript\"> "
      		+ "google.charts.load('current', {'packages':['corechart']});"
      		+ "google.charts.setOnLoadCallback(drawChart);"
      		+ "function drawChart() {"
      		+ "var data = google.visualization.arrayToDataTable(["
      		+ "['Task', 'Hours per Day'],"
      		+ "['BrokenLinks',     100],"
      		+ "['WorkingLinks',      200]]);"
      		//+ "['Commute',  2],"
      		//+ "['Watch TV', 2],"
      		//+ "['Sleep',    7]]); " 
      		+ " var options = { "
            + " title: 'My Daily Activities'};"
            + " var chart = new google.visualization.PieChart(document.getElementById('piechart')); "
            + "chart.draw(data, options); "
            + "} "
            + "</script>");
      bw1.write("</head>");
      bw1.write("<body><div id=\"piechart\" style=\"width: 900px; height: 500px;\"></div</body>");
      bw1.write("/html");
      bw1.close();
      
      
      
   }
}