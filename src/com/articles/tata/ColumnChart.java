package com.articles.tata;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class ColumnChart {

	public static void main(String[] args) throws IOException {
		
		 BufferedWriter bw1 = new BufferedWriter(new FileWriter("C:\\Users\\Amod Mahajan\\workspace\\Broken_Links_Tata_Articles\\ColumnChart.html"));
	      bw1.write("<html>");
	      bw1.write("<head>");
	      bw1.write("<script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>");
	      bw1.write("<script type=\"text/javascript\"> "
	      		+ "google.charts.load(\"current\", {packages:['corechart']});"
	      		+ "google.charts.setOnLoadCallback(drawChart);"
	      		+ "function drawChart() {"
	      		+ "var data = google.visualization.arrayToDataTable(["
	      		+ "[\"Element\", \"Density\", { role: \"style\" } ],"
	      		+ "[\"Copper\", 8.94, \"#b87333\"],"
	      		+ "[\"Platinum\", 21.45, \"color: #e5e4e2\"]]);"
	      		+ "var view = new google.visualization.DataView(data); "
	            + "view.setColumns([0, 1,"
	      		+ "{ calc: \"stringify\","
	            + " sourceColumn: 1,"
	      		+ " type: \"string\","
	      		+ " role: \"annotation\" },"
	      		+ " 2]);"
	      		+ " var options = { "
	            + " title: \"Density of Precious Metals, in g/cm^3\","
	      		+ " width: 600,"
	            + " height: 400,"
	      		+ " bar: {groupWidth: \"95%\"},"
	            + " };"
	            + " var chart = new google.visualization.ColumnChart(document.getElementById(\"columnchart_values\")); "
	            + "chart.draw(view, options); "
	            + "}"
	            + "</script>");
	      bw1.write("</head>");
	      bw1.write("<body><div id=\"columnchart_values\" style=\"width: 900px; height: 500px;\"></div</body>");
	      bw1.write("/html");
	      bw1.close();
	}
}
