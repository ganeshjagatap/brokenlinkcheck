package GeneralMethods;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProjectSpecificMethods {

	
	
	public static List<String> GenerateCSVFileFromBlog(WebDriver driver) throws Exception
	{
		driver.get("https://test.tatahealth.com/blog/wp-admin/");
		Thread.sleep(3000);
		driver.findElement(By.id("user_login")).sendKeys("admin");
		driver.findElement(By.id("user_pass")).sendKeys("Pass@1234");
		driver.findElement(By.id("wp-submit")).click();
		WebElement setting= driver.findElement(By.xpath("//div[text()='Settings']"));
		//Actions action= new Actions(driver);
		//action.moveToElement(setting).perform();
		setting.click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//a[text()='Export All URLs']")).click();
		driver.findElement(By.xpath("//input[@value='any']")).click();
		driver.findElement(By.xpath("//input[@value='url']")).click();
		driver.findElement(By.xpath("//input[@value='text']")).click();
		driver.findElement(By.xpath("//input[@value='Export']")).click();
		WebDriverWait wait= new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//strong[text()='Click here']"))).click();;
		driver.get("chrome://downloads/");
		System.out.println("current window handle:"+driver.getWindowHandle());
		WebElement root1 = driver.findElement(By.tagName("downloads-manager"));
		System.out.println(root1);
		WebElement shadowRoot1 = ReUsableMethods.expandRootElement(root1, driver);
		System.out.println(shadowRoot1);
		
		WebElement root2 = shadowRoot1.findElement(By.cssSelector("downloads-item"));
		System.out.println(root2);
		WebElement shadowRoot2 = ReUsableMethods.expandRootElement(root2, driver);
		
		//WebElement root3 = shadowRoot2.findElement(By.cssSelector("downloads-item"));
		//System.out.println(root3);
		//WebElement shadowRoot3 = expandRootElement(root3);
		WebElement e= shadowRoot2.findElement(By.partialLinkText("Exported_Data"));
		System.out.println("File name is:"+e.getText());
		String userDir= System.getProperty("user.dir");
		//C:\Users\Amod Mahajan\workspace\Copy of Broken_Links_Tata_Articles
		int n=StringUtils.ordinalIndexOf(userDir, "\\", 3);
		System.out.println(userDir.substring(0, n));
		String downloadPath=userDir.substring(0, n);
		
		List<String> urls= ReUsableMethods.readAllDataOfColumn(new File(downloadPath+"\\Downloads\\"+e.getText()));
		return urls;
		
	}
	
	
	
}
